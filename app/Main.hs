{-# LANGUAGE DataKinds #-}
module Main where

import MyPrelude
import Formula.CNF
import Parse
import Resolver
import SFStrategy

import qualified Options.Declarative as Opt

main :: IO ()
main = Opt.run_ $
  Opt.Group "simple resolver"
    [ Opt.subCmd "check" checkProblem
    , Opt.subCmd "solve" solveProblem
    ]


parseFile :: String -> IO (Either String CNF)
parseFile = fmap (fromTPTPCNF =<<) . parseCNF

solveProblem :: Opt.Flag "t" '["timeout"] "TIMEOUT" "timeout in seconds" (Opt.Def "0" Int)
  -> Opt.Arg "FILE" String
  -> Opt.Cmd "solve problem by resolution" ()
solveProblem tout filename = liftIO $
  parseFile (Opt.get filename) >>= \case
    Left msg -> error msg
    Right cnf -> execResolver resolver cnf >>= print
  where
    resolver :: ResolverM SmallestFirst IO SATUNSAT
    resolver = newResolver




checkProblem :: Opt.Arg "FILE" String
  -> Opt.Cmd "check if FILE is in correct format" ()
checkProblem filename = liftIO $
  parseFile (Opt.get filename) >>= \case
    Left msg -> putStrLn $ "wrong format: " ++ msg
    Right _  -> putStrLn "file is in correct format"
