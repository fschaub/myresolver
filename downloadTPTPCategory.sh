#!/bin/bash

domain="$1"
tptpurl="http://www.tptp.org/cgi-bin"

mkdir "$domain"

function cleanFromHTML {
  echo "cleaning proplem files from HTML"
  for f in `cat /dev/stdin`; do
    # isolate the embedded problem
    sed -i -n '/.*<pre>.*/','/.*<\/pre>.*/p' "$f"
    sed -i 's|<\/*pre>||g' "$f"  
    # remove links in include statements
    sed -i 's|<a href=[^>]*>\([^<]*\)</a>|\1|g' "$f"
    # remove tags around statements
    sed -i -e 's|<A[^>]*>||g' -e 's|</A>||g' "$f"
  done
}


echo "downloading problems of domain $domain"
curl "$tptpurl/SeeTPTP?Category=Problems&Domain=$domain" \
  | sed 's|^.*<a href=\([^>]*\.p\)>.*$|\1|g' \
  | grep -e '^SeeTPTP' \
  | while read url; do
      echo "downloading $url"
      fname=`grep -o -e '[^&=]*\...*' <<< "$url"`
      [[ ! -f "$domain/$fname" ]] && curl "$tptpurl/$url" > "$domain/$fname"
    done 
 

echo "downloading dependencies"
#mkdir "$domain/Axioms"
for f in $domain/*.p; do
  while read dep; do
    if [[ ! -z "$dep" ]] ; then
      echo "dependency: $dep"
      relurl=`sed -e 's|.*\(SeeTPTP[^>]*\)>.*|\1|g' <<< "$dep"`
      url="$tptpurl/$relurl"
      relfilename=`sed "s|^.*include('\([^<]*\)<[^>]*>\([^<]*\).*$|\1\2|g" <<< "$dep"`
      filename=`basename "$relfilename"`
      filepath=$domain/`dirname "$relfilename"`
      if [[ ! -f $filepath/$filename ]] ; then
        echo "downloading $url to $filepath/$filename"
        mkdir -p "$filepath"
        curl "$url" > "$filepath/$filename"
      fi
    fi
  done <<< `grep -e '^include.*$' "$f"`
done


find $domain -name "*.p" | cleanFromHTML
find $domain -name "*.ax" | cleanFromHTML
