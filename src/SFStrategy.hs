module SFStrategy
    ( SmallestFirst(..)
    ) where

import MyPrelude
import Formula.CNF

import qualified Resolver
import qualified Control.Monad.State as ST
import qualified Data.Vector as Vector
import qualified Data.PSQueue as PSQueue
newtype SmallestFirst = SF { queue :: PSQueue.PSQ Int Float }

instance Resolver.Strategy SmallestFirst where
  peek = Resolver.getStrategy >>= \s ->
    case PSQueue.findMin (queue s) of
      Nothing -> error "cannot peek empty queue"
      Just i  -> pure $ PSQueue.key i
  dequeue = do
    strat <- Resolver.getStrategy
    case PSQueue.minView (queue strat) of
      Nothing -> error "cannot dequeue empty queue"
      Just (i, nq)  -> do
        Resolver.setStrategy $ strat { queue = nq}
        pure $ PSQueue.key i
  enqueue i = do
    prio <- priority i
    strat <- Resolver.getStrategy
    Resolver.setStrategy $ strat { queue = PSQueue.insert i prio $ queue strat }
  delete i = do
    strat <- Resolver.getStrategy
    Resolver.setStrategy $ strat { queue = PSQueue.delete i $ queue strat }
  isNull = PSQueue.null . queue <$> Resolver.getStrategy
  size = PSQueue.size . queue <$> Resolver.getStrategy
  empty = SF { queue = PSQueue.empty }

priority :: (Monad m) => Int -> Resolver.ResolverM SmallestFirst m Float
priority i = fromIntegral . clauseSize <$> Resolver.getClause i










--
