module Parse
    ( parseCNF
    ) where

import MyPrelude
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import qualified Data.TPTP as TPTP
import qualified Data.TPTP.Parse.Text as TPTP
import qualified Data.List.NonEmpty as NE
import qualified System.Directory as SD
import qualified System.FilePath.Posix as FP

parseCNF :: FilePath -> IO (Either String [TPTP.Formula])
parseCNF fp = do
  cwd <- SD.getCurrentDirectory
  SD.setCurrentDirectory basepath
  -- print basepath
  -- print filename
  cnfE <- parseCNF_ filename Nothing
  SD.setCurrentDirectory cwd
  return cnfE
  where
    basepath = FP.takeDirectory fp
    filename = FP.takeFileName fp


parseCNF_ :: FilePath -> Maybe (NE.NonEmpty TPTP.UnitName) -> IO (Either String [TPTP.Formula])
parseCNF_ fp unames = do
  f <- Text.readFile fp
  parseUnits f
  where
    parseUnits :: Text.Text -> IO (Either String [TPTP.Formula])
    parseUnits f = case TPTP.units <$> TPTP.parseTPTPOnly f of
      Left err    -> return $ Left err
      Right units -> foldr' (liftA2 (++)) (pure []) <$> mapM parseUnit units
    parseUnit :: TPTP.Unit -> IO (Either String [TPTP.Formula])
    parseUnit (TPTP.Include (TPTP.Atom fp) maybeUnitnames) =
        parseCNF_ (Text.unpack fp) maybeUnitnames
    parseUnit (TPTP.Unit uname dec _) -- Annotations can be ignored ??
      | isNeededUnit uname = return $ case dec of
          TPTP.Formula _ (TPTP.CNF clause) -> Right [TPTP.CNF clause]
          _                                -> Left "encountered non-CNF Unit"
      | otherwise = return $ Right []
    isNeededUnit uname
      | isNothing unames = True
      | otherwise        = let (Just uns) = unames in uname `elem` uns
