module ClausePool
    ( ClausePool(..)
    , isProcessed
    , isUnprocessed
    , setProcessed
    , getProcessed
    , getUnprocessed
    , getClause
    , addClause
    , addClauses
    , fromCNF
    ) where


import MyPrelude
import Formula.CNF
import qualified Data.Vector as Vector
import qualified Data.HashSet as HashSet


-- TODO: change clauses to Set and dont add duplicates
data ClausePool = ClausePool
  { clauses :: Vector.Vector Clause
  , clauseSet :: HashSet.HashSet Clause
  , processed :: HashSet.HashSet Int
  , unprocessed :: HashSet.HashSet Int
  }
instance Semigroup ClausePool where
  x <> y =
    let
      offst = Vector.length $ clauses x
      newup = unprocessed x `HashSet.union` HashSet.map (+offst) (unprocessed y)
      newp_ = processed x `HashSet.union` HashSet.map (+offst) (processed y)
      newp = newp_ `HashSet.difference` newup
      newcs = clauseSet x `HashSet.union` clauseSet y
    in
      ClausePool
        { clauses = clauses x Vector.++ clauses y
        , clauseSet = newcs
        , processed = newp
        , unprocessed = newup
        }
instance Monoid ClausePool where
  mempty = ClausePool
    { clauses = mempty
    , clauseSet = mempty
    , processed = mempty
    , unprocessed = mempty
    }
  mappend = (<>)


getClause :: Int -> ClausePool -> Clause
getClause i cp = clauses cp Vector.! i

getUnprocessed :: ClausePool -> [(Int,Clause)]
getUnprocessed cp = (id &&& (clauses cp Vector.!)) <$> HashSet.toList (unprocessed cp)

getProcessed :: ClausePool -> [(Int,Clause)]
getProcessed cp = (id &&& (clauses cp Vector.!)) <$> HashSet.toList (processed cp)

setProcessed :: Int -> ClausePool -> ClausePool
setProcessed i cp
  | i `HashSet.member` unprocessed cp = cp
      { processed = i `HashSet.insert` processed cp
      , unprocessed = i `HashSet.delete` unprocessed cp
      }
  | otherwise = cp

isProcessed, isUnprocessed :: Int -> ClausePool -> Bool
isProcessed i cp = i `HashSet.member` processed cp
isUnprocessed i cp = i `HashSet.member` unprocessed cp

numProcessed :: ClausePool -> Int
numProcessed cp = HashSet.size $ processed cp
numUnprocessed :: ClausePool -> Int
numUnprocessed cp = HashSet.size $ unprocessed cp

size :: ClausePool -> Int
size cp = Vector.length $ clauses cp

addClause :: Clause -> ClausePool -> (ClausePool, Maybe Int)
addClause c cp
  | c `HashSet.member` clauseSet cp = (cp, Nothing)
  | otherwise =
    let
      idx = Vector.length $ clauses cp
      cp2 = cp
        { clauses = clauses cp `Vector.snoc` c
        , unprocessed = idx `HashSet.insert` unprocessed cp
        , clauseSet = c `HashSet.insert` clauseSet cp
        }
    in
      (cp2, pure idx)

addClauses :: Foldable f => f Clause -> ClausePool -> (ClausePool, [Int])
addClauses cls cp = second (catMaybes . reverse) $ foldl' mkCP (cp, mempty) cls
  where
    mkCP (cp, idxs) c = second (:idxs) $ addClause c cp

fromCNF :: CNF -> (ClausePool, [Int])
fromCNF (CNF cls) = addClauses cls mempty












--
