module Resolver
    ( SATUNSAT(..)
    , Resolver(..)
    , Strategy(..)
    , ResolverM(..)
    , getStrategy
    , setStrategy
    , getClause
    , newResolver
    , newResolverIO
    , execResolver
    , resolve
    ) where


import MyPrelude
import Formula.CNF
import Formula.Unification
import Formula.Pretty
import qualified ClausePool

import qualified Data.Vector as Vector
import qualified Control.Monad.State as ST



-- |satisfiability result
data SATUNSAT = SAT | UNSAT
  deriving (Show, Eq)
-- |the resolver datatype
data Resolver a = Resolver
  { clausePool :: ClausePool.ClausePool
  , strategy :: a
  }

-- |state monad using Resolver
type ResolverM a m c = ST.StateT (Resolver a) m c

-- |the strategy of the resolver
class Strategy a where
  peek :: (Monad m) => ResolverM a m Int
  dequeue :: (Monad m) => ResolverM a m Int
  enqueue :: (Monad m) => Int -> ResolverM a m ()
  delete :: (Monad m) => Int -> ResolverM a m ()
  isNull :: (Monad m) => ResolverM a m Bool
  size :: (Monad m) => ResolverM a m Int
  empty :: a


runResolverM :: (Strategy a, Monad m) => CNF -> ResolverM a m c -> m c
runResolverM cnf f = ST.evalStateT (initState cnf >> f) emptyState
  where
    emptyState = Resolver
      { clausePool = mempty
      , strategy = empty
      }

initState :: (Strategy a, Monad m) => CNF -> ResolverM a m ()
initState cnf = do
  let (cp, idxs) = ClausePool.fromCNF cnf
  ST.modify $ \s -> s { clausePool = cp , strategy = empty}
  mapM_ enqueue idxs

getClause :: (Strategy a, Monad m) => Int -> ResolverM a m Clause
getClause idx = ST.gets $ ClausePool.getClause idx . clausePool

setProcessed :: (Strategy a, Monad m) => Int -> ResolverM a m ()
setProcessed idx = ST.modify $ \s -> s
  { clausePool = ClausePool.setProcessed idx $ clausePool s
  }

getProcessed :: (Strategy a, Monad m) => ResolverM a m [(Int, Clause)]
getProcessed = ST.gets $ ClausePool.getProcessed . clausePool

getUnprocessed :: (Strategy a, Monad m) => ResolverM a m [(Int, Clause)]
getUnprocessed = ST.gets $ ClausePool.getUnprocessed . clausePool

addClauses :: (Strategy a, Foldable f, Monad m) => f Clause -> ResolverM a m ()
addClauses cs = do
  st <- ST.get
  let (newcp, idxs) = ClausePool.addClauses cs $ clausePool st
  ST.put $ st { clausePool = newcp }
  mapM_ enqueue idxs

getStrategy :: (Strategy a, Monad m) => ResolverM a m a
getStrategy = ST.gets strategy
setStrategy :: (Strategy a, Monad m) => a -> ResolverM a m ()
setStrategy s = ST.modify $ \r -> r { strategy = s }

processClause :: (Strategy a, Monad m) => Int -> ResolverM a m [Clause]
processClause idx = do
  c <- getClause idx
  ps <- getProcessed
  let resolvs = concatMap (resolve c . snd) ps
  addClauses resolvs
  setProcessed idx
  return resolvs




-- | resolve two clauses on all possible collisions
-- TODO: this can be much faster than O(n*m)
resolve :: Clause -> Clause -> [Clause]
resolve (Clause lits1) (Clause lits2) =
  fmap (Clause . Vector.fromList . factor) . catMaybes . concat $ resolv lits1 <$> Vector.toList lits2
    where
      resolv :: Vector.Vector (Sign, Lit) -> (Sign, Lit) -> [Maybe (Vector.Vector (Sign, Lit))]
      resolv xs y = resolv1 y <$> Vector.toList xs
      resolv1 (signx, x) (signy, y)
        | signx == signy = Nothing
        | otherwise = case unifier x y of
            Nothing     -> Nothing
            Just sigma  -> Just $ (rm x lits1 <> rm y lits2) ^$ sigma
      rm x = Vector.filter ((/= x) . snd)




newResolver, runRes :: (Strategy a, Monad m) => ResolverM a m SATUNSAT
newResolver = dequeue >>= setProcessed >> runRes
runRes = dequeue >>= processClause >>= loop
  where
    loop cs
      | any isEmptyClause cs = return UNSAT
      | otherwise = isNull >>= \b -> if b then return SAT else runRes


newResolverIO, runResIO :: (Strategy a, MonadIO m) => ResolverM a m SATUNSAT
newResolverIO = dequeue >>= setProcessed >> runResIO
runResIO = do
  ps <- map snd <$> getProcessed
  liftIO $ putStrLn $ "processed clauses: \n" ++ unlines (prettyF <$> ps)
  -- ups <- map snd <$> getUnprocessed
  -- liftIO $ putStrLn $ "unprocessed clauses: \n" ++ unlines (prettyF <$> ups)
  i <- dequeue
  c <- getClause i
  liftIO $ putStrLn $ "processing clause: \n" ++ prettyF c
  cs <- processClause i
  liftIO $ putStrLn $ "new clauses: \n" ++ unlines (prettyF <$> cs)
  loop cs
  where
    loop cs
      | any isEmptyClause cs = return UNSAT
      | otherwise = isNull >>= \b -> if b then return SAT else runResIO


execResolver :: (Strategy a, Monad m) => ResolverM a m SATUNSAT -> CNF -> m SATUNSAT
execResolver r cnf = runResolverM cnf r











--
