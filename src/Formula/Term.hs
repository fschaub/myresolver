{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Formula.Term
    ( Term(..)
    ) where

import MyPrelude
import Formula.Unification

import qualified Data.Vector as Vector
import qualified Data.Text as Text
import qualified Data.TPTP as TPTP



data Term = Fun Text.Text (Vector.Vector Term) | Var Text.Text
  deriving (Eq, Show, Ord, Generic, Hashable)


instance Unify Term where
  eqHead (Fun x1 args1) (Fun x2 args2)  = x1 == x2
  eqHead x y                            = x == y
  tails (Fun _ args1) = Vector.toList args1
  tails _             = []
  isVar (Var _) = True
  isVar _       = False
  continue (Fun x1 args1) f = Fun x1 $ fmap f args1
  continue x f              = x
instance AdmitsUnification Term Term where
  unifier = mgu
  (^$) = (.$)
instance (Functor f, Foldable f) => AdmitsUnification (f Term) Term where
  unifier = foldUnify
  (^$) xs sigma = fmap (^$ sigma) xs

--
