module Formula.Pretty where

import MyPrelude
import Formula.CNF
import Formula.Term
import qualified Data.Text as Text
import qualified Data.Vector as Vector

class PrettyF a where
  prettyF :: a -> String
instance PrettyF Sign where
  prettyF Pos = ""
  prettyF Neg = "-"
instance PrettyF Lit where
  prettyF (Predicate n ts) = Text.unpack n ++ prettyF ts
instance (PrettyF a, PrettyF b) => PrettyF (a,b) where
  prettyF (s,l) = prettyF s ++ prettyF l
instance PrettyF Clause where
  prettyF (Clause lits)
    | Vector.null lits = "[]" 
    | otherwise = prettyF lits
instance PrettyF CNF where
  prettyF (CNF cs) = prettyF cs
instance (PrettyF a) => PrettyF [a] where
  prettyF ts
    | null ts = ""
    | otherwise = "(" ++ foldl1 (\x y -> x ++ ", " ++ y) (prettyF <$> ts) ++ ")"
instance (PrettyF a) => PrettyF (Vector.Vector a) where
  prettyF = prettyF . toList
instance PrettyF Term where
  prettyF (Var x) = Text.unpack x
  prettyF (Fun f xs) = Text.unpack f ++ prettyF xs
