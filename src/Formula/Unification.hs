module Formula.Unification
    ( Substitution(..)
    , (:->)(..)
    , Unify(..)
    , (.$)
    , unify
    , unifiable
    , foldUnify
    , factor
    , mapSubstitutions
    , AdmitsUnification(..)
    , Hashable(..)
    , Generic(..)
    ) where


import MyPrelude
import qualified Data.Vector as Vector
import qualified Data.HashMap.Strict as HashMap
import Data.Hashable (Hashable, hash, hashWithSalt)
import GHC.Generics (Generic)

data a :-> b = a :-> b
  deriving (Show)


newtype Substitution a = Sigma (HashMap.HashMap a a)

instance Show a => Show (Substitution a) where
  show (Sigma m) = "Sigma := " ++ show (map (uncurry (:->)) (HashMap.toList m))



singleton :: (Hashable a) => (a :-> a) -> Substitution a
singleton (x :-> y) = Sigma $ HashMap.singleton x y

pack :: (Foldable f, Unify a, Eq a) => f (a :-> a) -> Substitution a
pack = foldl' (\s x -> s <> singleton x) mempty

unpack :: Substitution a -> [a :-> a]
unpack (Sigma m) = uncurry (:->) <$> HashMap.toList m

substitute1 :: (Hashable a, Eq a) => Substitution a -> a -> a
substitute1 (Sigma s) x = HashMap.findWithDefault x x s

mapSubstitutions :: (Hashable b, Unify b, Eq b, Monoid [b]) => ((a :-> a) -> (b :-> b)) -> Substitution a -> Substitution b
mapSubstitutions f = pack . fmap f . unpack


{- | composition of Substitutions
-- composition of substitutions θ = {x1 → t1, . . . , xn → tn} and σ = {y1 → s1, . . . , yk → sk } is
-- substitution θσ = {x1 → t1σ, . . . , xn → tnσ } ∪ {yi → si | yi /= xj for all 1 <= j <= n}
-}
instance (Unify a, Hashable a, Eq a) => Semigroup (Substitution a) where
  (Sigma xs) <> (Sigma ys) =
    let
      nxs = (.$ Sigma ys) <$> xs
    in
      Sigma $ nxs `HashMap.union` ys

instance (Unify a, Hashable a , Eq a) => Monoid (Substitution a) where
  mempty = Sigma HashMap.empty
  mappend = (<>)

-- | type a admits unification where substituions are
-- | applied on type b
class (Hashable b, Eq b, Unify b) => AdmitsUnification a b | a -> b where
  unifier :: a -> a -> Maybe (Substitution b)
  (^$) :: a -> Substitution b -> a


unify :: (AdmitsUnification a b) => a -> a -> Maybe a
unify x y = (x ^$) <$> unifier x y
unifiable :: (AdmitsUnification a b) => a -> a -> Bool
unifiable x y = isJust $ unifier x y

foldUnify :: (AdmitsUnification a b, Eq b, Foldable f) => f a -> f a -> Maybe (Substitution b)
foldUnify xs ys = fUnify (pure mempty) $ zip (toList xs) (toList ys)
  where
    fUnify :: (AdmitsUnification a b, Eq b) => Maybe (Substitution b) -> [(a,a)] -> Maybe (Substitution b)
    fUnify = foldl' $ \msigma (x,y) -> do
      xsig <- (x ^$) <$> msigma
      ysig <- (y ^$) <$> msigma
      (<>) <$> unifier xsig ysig <*> msigma

-- | infix substituion application
(.$) :: (Unify a) => a -> Substitution a -> a
(.$) = flip substitute

-- | remove duplicates by Unification
factor :: (AdmitsUnification a b, Foldable m) => m a -> [a]
factor = foldl' insertNotUnifyable mempty
  where
    insertNotUnifyable :: (AdmitsUnification a b) => [a] -> a -> [a]
    insertNotUnifyable acc x = inu x acc
    inu :: (AdmitsUnification a b) => a -> [a] -> [a]
    inu x [] = [x]
    inu x (a:acc) = case unifier x a of
      Just sigma -> (x ^$ sigma) : acc
      Nothing    -> a : inu x acc

class (Hashable f, Eq f) => Unify f where
  -- | equality of the head symbols
  eqHead :: f -> f -> Bool
  -- | the arguments of a function
  tails :: f -> [f]
  -- | check if f is a variable
  isVar :: f -> Bool
  -- | continuous application of a function over the datatype
  continue :: f -> (f -> f) -> f
  -- | the most general unifier
  mgu :: f -> f -> Maybe (Substitution f)
  mgu x y
    | x == y      = pure mempty
    | isVar x
    && not (oc x y) = pure $ singleton $ x :-> y
    | isVar y
    && not (oc y x) = pure $ singleton $ y :-> x
    | eqHead x y  = case (tails x,tails y) of
        ([],[])  -> pure mempty
        (xs,ys)  ->
          if length xs /= length ys then
            error "inconsistent function instances found: different arities"
          else
            let
              fUnify = foldl' $ \msigma (x,y) -> do
                xsig <- (x .$) <$> msigma
                ysig <- (y .$) <$> msigma
                (<>) <$> msigma <*> mgu xsig ysig
            in
              fUnify (pure mempty) $ zip xs ys
    | otherwise   = Nothing
    where
      -- | occurs check if x is in y
      oc x y = x == y || any (oc x) (tails y)
  -- | apply the substitution
  substitute :: Substitution f -> f -> f
  substitute sigma f
    | isVar f  = substitute1 sigma f
    | otherwise = continue f (substitute sigma)

instance Hashable a => Hashable (Vector.Vector a) where
  hash = hash . Vector.toList
  hashWithSalt i = hashWithSalt i . Vector.toList
