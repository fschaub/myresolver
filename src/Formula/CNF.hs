{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Formula.CNF
    ( Sign(..)
    , Lit(..)
    , Clause(..)
    , isEmptyClause
    , clauseSize
    , CNF(..)
    , fromTPTPCNF
    , testCNF
    ) where

import MyPrelude
import Formula.Term
import Formula.Unification

import qualified Data.Vector as Vector
import qualified Data.Hashable as Hashable

import qualified Data.Text as Text
import qualified Data.TPTP as TPTP
import qualified Data.List.NonEmpty as NE


-- | Sign of a literal
data Sign = Pos | Neg
  deriving (Eq, Show, Generic, Hashable)

-- | Literals are Predicates with some terms
data Lit = Predicate Text.Text (Vector.Vector Term)
  deriving (Eq, Show, Generic, Hashable)

-- | Clauses are sets of signed literals
newtype Clause = Clause (Vector.Vector (Sign, Lit))
  deriving (Eq, Show, Generic, Hashable)

isEmptyClause :: Clause -> Bool
isEmptyClause (Clause v) = Vector.null v

clauseSize :: Clause -> Int
clauseSize (Clause v) = Vector.length v

-- | CNF is a set of Clauses
newtype CNF = CNF (Vector.Vector Clause)
  deriving (Eq, Show)


fromTPTPCNF :: [TPTP.Formula] -> Either String CNF
fromTPTPCNF fs = CNF . Vector.fromList <$> mapM clause fs
  where
    clause :: TPTP.Formula -> Either String Clause
    clause (TPTP.CNF (TPTP.Clause cl)) = Clause . Vector.fromList . NE.toList <$> mapM (signlit . first sign) cl
    sign TPTP.Positive = Pos
    sign TPTP.Negative = Neg
    signlit (sig, TPTP.Predicate n ts) =
      pure (sig, Predicate (Text.pack $ show n) . Vector.fromList $ term <$> ts)
    signlit (sig, TPTP.Equality _ _ _) = fail "equality is currently not supported"
    -- | no equality is currently supported since we first need to add equality axiomsc
    -- signlit (sig, TPTP.Equality t1 s t2) =
    --   let nsig = case (sig, sign s) of
    --         (Pos, Neg) -> Neg
    --         (Neg, Neg) -> Pos
    --         (_  , Pos) -> sig
    --   in (nsig, Predicate "reserved_equality" $ Vector.fromList $ sort $ fmap term [t1, t2])
    term (TPTP.Function name ts)                      = Fun (Text.pack $ show name) $ Vector.fromList $ fmap term ts
    term (TPTP.Number n)                              = Fun (Text.pack $ show n) mempty
    term (TPTP.DistinctTerm (TPTP.DistinctObject x))  = Fun x mempty
    term (TPTP.Variable (TPTP.Var var))               = Var var


instance AdmitsUnification Lit Term where
  unifier (Predicate x as) (Predicate y bs)
    | x == y    = foldUnify (toList as) (toList bs)
    | otherwise = Nothing
  (^$) (Predicate x as) sigma = Predicate x $ substitute sigma <$> as

instance AdmitsUnification (Sign, Lit) Term where
  unifier (sp,p) (sq, q)
    | sp == sq = unifier p q
    | otherwise = Nothing
  (^$) (s,p) sigma = (s, p ^$ sigma)
instance (Foldable f, Functor f) => AdmitsUnification (f (Sign,Lit)) Term where
  unifier = foldUnify
  (^$) xs sigma = fmap (^$ sigma) xs
instance AdmitsUnification Clause Term where
  unifier (Clause xs) (Clause ys) = foldUnify xs ys
  (^$) (Clause xs) sigma = Clause $ xs ^$ sigma



testCNF :: CNF
testCNF = CNF $ Vector.fromList
  [ Clause $ Vector.fromList
    [ (,) Pos $ Predicate "A" $ Vector.fromList
      [ Fun "f" $ Vector.fromList
        [ Var "x"
        , Var "y"
        ]
      , Fun "g" $ Vector.fromList
        [ Var "y"
        , Var "x"
        ]
      ]
    ]
  , Clause $ Vector.fromList
    [ (,) Pos $ Predicate "A" $ Vector.fromList
      [ Fun "f" $ Vector.fromList
        [ Fun "1" Vector.empty
        , Fun "2" Vector.empty
        ]
      , Fun "g" $ Vector.fromList
        [ Fun "1" Vector.empty
        , Fun "2" Vector.empty
        ]
      ]
    ]
  , Clause $ Vector.fromList
    [ (,) Pos $ Predicate "B" $ Vector.fromList
      [ Fun "f" $ Vector.fromList
        [ Var "z"
        , Var "v"
        ]
      , Fun "g" $ Vector.fromList
        [ Var "v"
        , Var "z"
        ]
      ]
    ]
  , Clause $ Vector.fromList
    [ (,) Neg $ Predicate "A" $ Vector.fromList
      [ Fun "f" $ Vector.fromList
        [ Var "z"
        , Var "v"
        ]
      , Fun "g" $ Vector.fromList
        [ Var "v"
        , Var "z"
        ]
      ]
    , (,) Pos $ Predicate "A" $ Vector.fromList
      [ Fun "f" $ Vector.fromList
        [ Var "z"
        , Var "v"
        ]
      , Fun "g" $ Vector.fromList
        [ Var "z"
        , Var "v"
        ]
      ]
    ]
  , Clause $ Vector.fromList
    [ (,) Pos $ Predicate "B" $ Vector.fromList
      [ Fun "f" $ Vector.fromList
        [ Fun "2" Vector.empty
        , Fun "1" Vector.empty
        ]
      , Fun "g" $ Vector.fromList
        [ Fun "1" Vector.empty
        , Fun "2" Vector.empty
        ]
      ]
    ]
  ]




--
