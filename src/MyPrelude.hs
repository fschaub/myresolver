module MyPrelude
    ( module Pre
    , module List
    , module Fold
    , module Either
    , module Maybe
    , module Monad
    , module Functor
    , module Applicative
    , module Arrow
    ) where

import Prelude as Pre
import Data.List as List (sort)
import Data.Foldable as Fold
import Data.Either as Either
import Data.Maybe as Maybe
import Control.Monad as Monad (forM, forM_, zipWithM)
import Control.Monad.Identity as Monad
import Control.Monad.IO.Class as Monad
import Data.Functor as Functor
import Data.Bifunctor as Functor (bimap)
import Control.Applicative as Applicative hiding (empty)
import Control.Arrow as Arrow ((&&&), (***), first, second)



instance MonadFail (Either String) where
  fail = Left
